;; jao-proton-utils.el -- simple interaction with Proton mail and vpn

;; Copyright (c) 2018, 2019, 2020 Jose Antonio Ortega Ruiz

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;; Author: Jose Antonio Ortega Ruiz <mail@jao.io>
;; Start date: Fri Dec 21, 2018 23:56

;;; Comentary:

;;  This is a very simple comint-derived mode to run the CLI version
;;  of PM's Bridge within the comfort of emacs.

;;; Code:

(define-derived-mode proton-bridge-mode comint-mode "proton-bridge"
  "A very simple comint-based mode to run ProtonMail's bridge"
  (setq comint-prompt-read-only t)
  (setq comint-prompt-regexp "^>>> "))

;;;###autoload
(defun run-proton-bridge ()
  "Run or switch to an existing bridge process, using its CLI"
  (interactive)
  (pop-to-buffer (make-comint "proton-bridge" "protonmail-bridge" nil "-c"))
  (unless (eq major-mode 'proton-bridge-mode)
    (proton-bridge-mode)))

(defvar proton-vpn-mode-map)

(defvar jao-proton-vpn-font-lock-keywords '("\\[.+\\]"))

;;;###autoload
(defun proton-vpn-mode ()
  "A very simple mode to show the output of ProtonVPN commands"
  (interactive)
  (kill-all-local-variables)
  (buffer-disable-undo)
  (use-local-map proton-vpn-mode-map)
  (setq-local font-lock-defaults '(jao-proton-vpn-font-lock-keywords))
  (setq-local truncate-lines t)
  (setq-local next-line-add-newlines nil)
  (setq major-mode 'proton-vpn-mode)
  (setq mode-name "proton-vpn")
  (read-only-mode 1))

(defvar jao-proton-vpn--buffer "*pvpn*")

(defun jao-proton-vpn--do (things)
  (let ((b (pop-to-buffer (get-buffer-create jao-proton-vpn--buffer))))
    (let ((inhibit-read-only t)
          (cmd (format "protonvpn-cli %s" things)))
      (delete-region (point-min) (point-max))
      (message "Running: %s ...." cmd)
      (shell-command cmd b)
      (message ""))
    (proton-vpn-mode)))

;;;###autoload
(defun proton-vpn-status ()
  (interactive)
  (jao-proton-vpn--do "s"))

(defun proton-vpn--get-status ()
  (or (when-let ((b (get-buffer jao-proton-vpn--buffer)))
        (with-current-buffer b
          (goto-char (point-min))
          (if (re-search-forward "^Status: *\\(.+\\)$" nil t)
              (match-string-no-properties 1)
            (when (re-search-forward "^Connected!$")
              "Connected"))))
      "Disconnected"))

;;;###autoload
(defun proton-vpn-connect (cc)
  (interactive "P")
  (let ((cc (when cc (read-string "Country code: "))))
    (jao-proton-vpn--do (if cc (format "c --cc %s" cc) "c --sc"))
    (proton-vpn-status)))

(defun proton-vpn-reconnect ()
  (interactive)
  (jao-proton-vpn--do "r"))

(setenv "PVPN_WAIT" "300")

;;;###autoload
(defun proton-vpn-maybe-reconnect ()
  (interactive)
  (when (string= "Connected" (proton-vpn--get-status))
    (jao-proton-vpn--do "d")
    (sit-for 5)
    (jao-proton-vpn--do "r")))

;;;###autoload
(defun proton-vpn-disconnect ()
  (interactive)
  (jao-proton-vpn--do "d"))

(setq proton-vpn-mode-map
      (let ((map (make-keymap)))
        (suppress-keymap map)
        (define-key map [?q] 'bury-buffer)
        (define-key map [?n] 'next-line)
        (define-key map [?p] 'previous-line)
        (define-key map [?g] 'proton-vpn-status)
        (define-key map [?r] 'proton-vpn-reconnect)
        (define-key map [?d] (lambda ()
                               (interactive)
                               (when (y-or-n-p "Disconnect?")
                                 (proton-vpn-disconnect))))
        (define-key map [?c] 'proton-vpn-connect)
        map))


(provide 'jao-proton-utils)
;;; jao-proton.el ends here
