;;; jao-counsel-recoll.el --- counsel and recoll     -*- lexical-binding: t; -*-

;; Copyright (C) 2020  jao

;; Author: jao <mail@jao.io>
;; Keywords: docs

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Helpers for using recoll with counsel

;;; Code:

(require 'jao-recoll)
(require 'counsel)
(require 'ivy)

(defvar jao-counsel-recoll--history nil)
(defun jao-counsel-recoll--function (str)
  (let ((xs (counsel-recoll-function str)))
    (cl-remove-if-not (lambda (x) (string-prefix-p "file://" x)) xs)))

;;;###autoload
(defun jao-counsel-recoll (&optional initial-input)
  (interactive)
  (counsel-require-program "recoll")
  (ivy-read "recoll: " 'jao-counsel-recoll--function
            :initial-input initial-input
            :dynamic-collection t
            :history 'jao-counsel-recoll--history
            :action (lambda (x)
                      (when (string-match "file://\\(.*\\)\\'" x)
                        (let ((file-name (match-string 1 x)))
                          (if (string-match "pdf$" x)
                              (jao-open-doc file-name)
                            (find-file file-name)))))
            :unwind #'counsel-delete-process
            :caller 'jao-counsel-recoll))

(defun jao-counsel-recoll--recoll (_s) (jao-recoll ivy-text))

(ivy-set-actions 'jao-counsel-recoll
                 '(("x" jao-counsel-recoll--recoll "List in buffer")))


(provide 'jao-counsel-recoll)
;;; jao-counsel-recoll.el ends here
