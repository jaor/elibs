(deftheme jao-doom
  "Created 2019-12-13.")

(defun jao-doom-color (c &optional alt)
  (let ((c (assoc c doom-themes--colors))) (if alt (caddr c) (cadr c))))
(defun jao-doom-face (f)
  (mapcar (lambda (x)
            (cond ((symbolp x) (or (jao-doom-color x) x))
                  ((listp x) (jao-doom-face x))
                  (t x)))
          (if (listp f) f (cdr (assoc f doom-themes--faces)))))

;; (enable-theme 'jao-doom)

(custom-theme-set-faces
 'jao-doom
 '(Info-quoted ((t (:inherit font-lock-variable-name-face))))
 '(custom-button ((t (:background "#282b33" :foreground "#819cd6" :box nil))))
 '(dictionary-reference-face ((t (:inherit (font-lock-keyword-face)))))
 '(dictionary-word-definition-face ((t (:inherit default))))
 `(error ((t (:foreground ,(jao-doom-color 'orange)))))
 `(diff-hl-change ((t (:inherit default :background "#313153"))))
 '(diff-hl-delete ((t (:inherit default :background "#533133"))))
 `(diff-hl-insert ((t (:inherit default :background "#315331"))))
 '(fill-column-indicator ((t (:foreground "grey25"))))
 `(gnus-summary-normal-read ((t (:foreground ,(jao-doom-color 'grey)))))
 `(gnus-summary-normal-unread ((t ,(jao-doom-face 'default))))
 `(highlight ((t (;; :underline ,(jao-doom-color 'green t)
                  :background ,(jao-doom-color 'bg-alt)))))
 `(gnus-summary-selected ((t (:inherit highlight))))
 '(lui-button-face ((t (:foreground "#7ebebd" :underline nil))))
 `(link-visited ((t (:foreground ,(jao-doom-color 'green)))))
 '(magit-diff-context-highlight ((t (:background "#333344"))))
 `(magit-diff-hunk-heading-highlight ((t (,@(jao-doom-face 'default)
                                          :overline nil :underline t :extend t))))
 '(magit-diff-removed-highlight ((t (:foreground "tan" :bold nil))))
 '(magit-diff-added-highlight ((t (:foreground "antiquewhite" :bold nil))))
 `(mode-line ((t (:foreground "#999999" ;; ,(jao-doom-color 'modeline-fg-alt)
                  :background ,(jao-doom-color 'modeline-bg)
                  :box (:line-width 2 :color ,(jao-doom-color 'modeline-bg))))))
 `(mode-line-inactive ((t (:foreground ,(jao-doom-color 'modeline-fg-alt t)
                           :background ,(jao-doom-color 'modeline-bg-inactive)
                           :box (:line-width 2 :color ,(jao-doom-color 'modeline-bg-inactive)))))) ;;  "#3a3a4a"
 '(mpdel-tablist-album-face ((t (:inherit font-lock-doc-face))))
 '(mpdel-tablist-artist-face ((t (:inherit font-lock-keyword-face))))
 '(org-block-begin-line ((t (:inherit font-lock-comment-face :extend nil))))
 '(org-block-end-line ((t (:inherit org-block-begin-line :extend nil))))
 `(scroll-bar ((t (:foreground ,(jao-doom-color 'modeline-bg)
                   :background ,(jao-doom-color 'bg)))))
 '(variable-pitch ((t (:inherit default))))
 '(w3m-form-button ((t (:inherit button)))))

(custom-theme-set-variables
 'jao-doom
 '(fci-rule-color "grey25"))

(provide-theme 'jao-doom)
