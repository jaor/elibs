(jao-define-custom-theme jao-light
  (:names (dimm-background "#f4f4f4")
          (dimm-background-2 "#f0f0f0")
          (dimm-background-3 "#f6f6f6")
          (dimm-background-4 "#fafafa")
          (yellowish-background "#fffff8")
          (link "#00552a")
          (yellow "#fdf6e3")
          (pale-yellow "#fff8e5")
          (paler-yellow "#fffff8")
          (green "#005555")
          (light-green "darkseagreen4")
          (greyish "#626262")
          (blueish "#1414aa")
          (blue "#819cd6")
          (blue2 "#51afef")
          (pale-blue "azure2")
          (dark-blue "#616c96")
          (dark-blue-1 "#2257A0")
          (dark-blue-2 "#023770")
          (keywords "lightsteelblue4")
          (keywords2 "#2257A0")
          (functions "#005555")
          (red "salmon3")
          (red2 "sienna4"))
  (:palette (fg unspecified "black")
            (bg unspecified "white")
            ;; (bg unspecified "#fffff8")
            (box "grey80" "antiquewhite3")
            (button ((c link) nit))
            (hilite ((c nil dimm-background)))
            (strike-through ((c 1)) (st))
            (italic (it))
            (link ((c dark-blue-2) nul nbf))
            (visited-link ((c dark-blue-1) nul nbf))
            (tab-sel ((~ mode-line)))
            (tab-unsel ((~ mode-line-inactive)))
            (comment ((c greyish) it))
            (keyword ((c dark-blue-2) nbf))
            (type ((c light-green) nbf))
            (function ((c green nil) nbf))
            (variable-name ((c "black")))
            (constant ((c 23)))
            (string ((c link)))
            (warning ((c red2)))
            (error ((c red)))
            (dimm ((c "lemonchiffon4")))
            (gnus-mail ((c "black")))
            (gnus-news ((c "black")))
            (outline ((c "black") nbf))
            (outline-1 ((c dark-blue-1) it bf))
            (outline-2 ((c functions) it nbf))
            (outline-3 ((c link) it nbf))
            (outline-4 ((c nil) it nbf))
            (outline-5 ((c nil)))
            (f00 ((c green)))
            (f01 ((c dark-blue-1)))
            (f02 ((c light-green)))
            (f10 ((p f00)))
            (f11 ((p f01)))
            (f12 ((p f02))))
  (:faces (mode-line (c nil dimm-background) ;; "ghost white"
                     :box (:line-width 1 :color "grey80"))
          (mode-line-inactive (c "grey40" dimm-background-2)
                              :box (:line-width 1 :color "grey85"))
          (mode-line-buffer-id (~ mode-line) nit)
          (mode-line-emphasis it)
          (mode-line-highlight (c green nil)))
  (:x-faces (bold (c "#223142") bf)
            (compilation-info (c "#223142" nil) nbf)
            (company-scrollbar-bg (c nil "grey95"))
            (company-scrollbar-fg (c nil "grey90"))
            (cursor (c "sienna3" "sienna3"))
            (diary (p error) nbf)
            (diff-hl-change (c "white" pale-blue))
            (diff-hl-insert (c "white" "honeydew2"))
            (diff-hl-delete (c "white" "wheat1"))
            (fill-column-indicator (c "grey80"))
            (fringe (c "grey70" nil))
            (gnus-button (p link))
            (gnus-summary-selected (c green) nbf)
            (gnus-summary-cancelled (c "sienna3") st)
            (header-line (c nil "#efebe7"))
            (ivy-highlight-face (c nil pale-yellow))
            (ivy-current-match (c nil pale-yellow))
            (lui-track-bar (p dimm) :height 0.2 nul nil ex)
            (magit-diff-context-highlight (c nil yellow) ex)
            (magit-diff-hunk-heading-highlight (c nil yellow) it bf)
            (mode-line (c "grey30" dimm-background-3) ;; "ghost white"
                       :box (:line-width 1 :color "grey90"))
            (mode-line-inactive (c "grey40" dimm-background-4)
                                :height 1
                                :box (:line-width 1 :color "grey90"))
            (mode-line-buffer-id (~ mode-line) (c dark-blue-2) nit)
            (mode-line-emphasis (c green nil))
            (mode-line-highlight (c green nil))
            (org-link (p link) ul)
            (scroll-bar (c "grey80"))
            (success (p f00))
            (vertical-border (c "grey70" nil))
            (warning (c "burlywood4"))
            (w3m-image (c "midnightblue" "azure2"))
            (w3m-bold (c "darkslategray") bf)
            (w3m-tab-selected (c "orangered4" "white") bf)
            (w3m-tab-selected-retrieving (~ w3m-tab-selected) (c 1))
            (w3m-tab-background (c "white" "white") nul)))

;; (enable-theme 'jao-light)

(provide 'jao-light-theme)
