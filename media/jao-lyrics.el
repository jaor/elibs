;; jao-lyrics.el -- simple show lyrics using glyrc

;; Copyright (C) 2009, 2010, 2017, 2019, 2020 Jose Antonio Ortega Ruiz

;; Author: Jose Antonio Ortega Ruiz <jao@gnu.org>
;; Start date: Sat Jul 04, 2009 13:41

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(defgroup jao-lyrics-faces nil "Faces"
  :group 'faces)

(defface jao-lyrics-font-lock-album '((t (:foreground "lightgoldenrod2")))
  "Album name in lyrics."
  :group 'jao-lyrics-faces)

(defface jao-lyrics-font-lock-title '((t (:foreground "dodgerblue2")))
  "Track title in lyrics."
  :group 'jao-lyrics-faces)

(defface jao-lyrics-font-lock-artist '((t (:foreground "dodgerblue3")))
  "Artist name in lyrics."
  :group 'jao-lyrics-faces)

(defvar jao-lyrics-cache-dir "~/.lyrics")

(defun jao-lyrics--filename (artist title)
  (expand-file-name (format "%s - %s.txt" artist title)
                    jao-lyrics-cache-dir))

(defun jao-lyrics--get-cached (artist title)
  (let ((candidate (jao-lyrics--filename artist title)))
    (and (file-exists-p candidate)
         (with-current-buffer (find-file-noselect candidate)
           (prog1
               (buffer-string)
             (kill-buffer))))))

(defun jao-lyrics--cache (artist title lyrics)
  (with-current-buffer
      (find-file-noselect (jao-lyrics--filename artist title))
    (delete-region (point-min) (point-max))
    (insert lyrics)
    (save-buffer)
    (kill-buffer)))

(make-variable-buffer-local
 (defvar jao-lyrics--path nil))

(defvar jao-lyrics-mode-map)
(setq jao-lyrics-mode-map
      (let ((map (make-keymap)))
        (suppress-keymap map)
        (define-key map [?q] 'bury-buffer)
        (define-key map [?g] 'jao-show-lyrics)
        (define-key map [?G] (lambda () (interactive) (jao-show-lyrics t)))
        (define-key map [?e] 'jao-edit-lyrics)
        map))

(defun jao-lyrics-mode ()
  (interactive)
  (kill-all-local-variables)
  (use-local-map jao-lyrics-mode-map)
  (setq major-mode 'jao-lyrics-mode)
  (setq mode-name "lyrics")
  (toggle-read-only 1))

(defun jao-lyrics-buffer ()
  (or (get-buffer "*Lyrics*")
      (with-current-buffer (get-buffer-create "*Lyrics*")
        (jao-lyrics-mode)
        (current-buffer))))

(defun jao-edit-lyrics ()
  (interactive)
  (unless jao-lyrics--path
    (error "No track data available."))
  (find-file-other-window jao-lyrics--path))



(defun jao-lyrics--clean-download (fn)
  (with-current-buffer (find-file-noselect fn)
    (goto-char (point-min))
    (when (re-search-forward
           "^\\(CreditsWritten by:\\|External linksNominate\\)" nil t)
      (beginning-of-line)
      (kill-region (point) (point-max)))
    (replace-string "" "" nil (point-min) (point-max))
    (replace-string "\\'" "'"  nil (point-min) (point-max))
    (save-buffer)))

(defun jao-lyrics--download (artist title &optional noartist)
  (message "Retrieving lyrics...")
  (or (executable-find "glyrc")
      (error "glyrc not installed"))
  (let ((fn (jao-lyrics--filename (or noartist artist) title)))
    (shell-command-to-string (format "glyrc lyrics -n 1-8 -Y -a %s -t %s -w %s"
                                     (shell-quote-argument artist)
                                     (shell-quote-argument title)
                                     (shell-quote-argument fn)))
    (jao-lyrics--clean-download fn)
    (prog1 (jao-lyrics--get-cached artist title) (message nil))))

(defvar jao-lyrics-info-function)
(defvar-local jao-lyrics--info-function nil)

;;;###autoload
(defun jao-show-lyrics (&optional force info-function)
  (interactive "P")
  (let* ((a/t (funcall (or info-function
                           jao-lyrics--info-function
                           jao-lyrics-info-function)))
         (artist (car a/t))
         (title (cdr a/t))
         (artist (if force (read-string "Artist: " artist) artist))
         (title (if force (read-string "Title: " title) title))
         (buffer (jao-lyrics-buffer))
         (cached (and (not force) (jao-lyrics--get-cached artist title)))
         (cached (and (not (zerop (length cached))) cached))
         (lyrics (or cached
                     (jao-lyrics--download artist title)
                     (jao-lyrics--download "" title artist)))
         (inhibit-read-only t))
    (with-current-buffer buffer
      (when info-function
        (setq-local jao-lyrics--info-function info-function))
      (delete-region (point-min) (point-max))
      (insert (format "♪ %s - %s\n\n"
                      (propertize artist 'face 'jao-lyrics-font-lock-artist)
                      (propertize title 'face 'jao-lyrics-font-lock-title)))
      (when lyrics (insert lyrics))
      (goto-char (point-min))
      (setq jao-lyrics--path (jao-lyrics--filename artist title)))
    (pop-to-buffer buffer)))


(provide 'jao-lyrics)
;;; jao-lyrics.el ends here
