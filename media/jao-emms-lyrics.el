;; jao-emms-lyrics.el -- simple show lyrics in emms

;; Copyright (C) 2009, 2010, 2017, 2019, 2020 Jose Antonio Ortega Ruiz

;; Author: Jose Antonio Ortega Ruiz <jao@gnu.org>
;; Start date: Sat Jul 04, 2009 13:41

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'emms)
(require 'jao-lyrics)

;;;###autoload
(defun jao-emms-lyrics-track-data ()
  (let ((track (or (emms-playlist-current-selected-track)
                   (error "No playing track"))))
    (cons (or (emms-track-get track 'info-artist nil)
              (error "No artist"))
          (or (emms-track-get track 'info-title nil)
              (error "No artist")))))

;;;###autoload
(defun jao-emms-show-lyrics (&optional force)
  (let ((jao-lyrics-info-function 'jao-emms-lyrics-track-data))
    (jao-show-lyrics force)))

(provide 'jao-emms-lyrics)
;;; jao-emms-lyrics.el ends here
