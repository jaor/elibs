;; jao-random-album.el -- play random albums

;; Copyright (C) 2009, 2010, 2017, 2018, 2019 Jose Antonio Ortega Ruiz

;; Author: Jose Antonio Ortega Ruiz <jao@gnu.org>
;; Start date: Sat Jul 04, 2009 13:06

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'jao-notify)

(defvar jao-random-album-p t)
(defvar jao-random-lines nil)
(defvar jao-random-lines-file (expand-file-name "~/.emacs.d/random-lines"))
(defvar jao-random-album-notify-p t)
(defvar jao-random-album-notify-icon nil)
(defvar jao-random-album-skip-lines 2)

(defun jao-random-lines ()
  (or jao-random-lines
      (and (file-exists-p jao-random-lines-file)
           (with-current-buffer
               (find-file-noselect jao-random-lines-file)
             (goto-char (point-min))
             (setq jao-random-lines (read (current-buffer)))))
      (dotimes (n (1- (line-number-at-pos (point-max)))
                  jao-random-lines)
        (when (> n jao-random-album-skip-lines)
          (push (1+ n) jao-random-lines)))))

(defun jao-random-lines-save ()
  (with-current-buffer (find-file-noselect jao-random-lines-file)
    (delete-region (point-min) (point-max))
    (insert (format "%s\n" jao-random-lines))
    (save-buffer)))

(defun jao-goto-random-album ()
  (let* ((pos (random (length (jao-random-lines))))
         (line (nth pos jao-random-lines)))
    (setq jao-random-lines (remove line jao-random-lines))
    (jao-random-lines-save)
    (goto-line line)))


;; User interface
(defvar jao-random-album-buffer)
(defvar jao-random-album-add-tracks-and-play)
(defvar jao-random-album-stop)

(defun jao-random-album-start ()
  (interactive)
  (setq jao-random-album-p t)
  (jao-random-album-next))

(defun jao-random-album-stop ()
  (interactive)
  (setq jao-random-album-p nil)
  (funcall jao-random-album-stop))

(defun jao-random-album-toggle ()
  (interactive)
  (setq jao-random-album-p (not jao-random-album-p))
  (message "Random album %s"
           (if jao-random-album-p "enabled" "disabled")))

(defun jao-random-album-next ()
  (interactive)
  (with-current-buffer (get-buffer (funcall jao-random-album-buffer))
    (save-excursion
      (jao-goto-random-album)
      (let ((album (string-trim
                    (substring-no-properties (thing-at-point 'line) 0 -1))))
        (funcall jao-random-album-add-tracks-and-play)
        (when jao-random-album-notify-p
          (jao-notify album "Next album" jao-random-album-notify-icon))))))

(defun jao-random-album-reset ()
  (interactive)
  (setq jao-random-lines nil)
  (jao-random-lines-save))

(defun jao-random-album-setup (album-buffer add-and-play stop &optional icon)
  (setq jao-random-album-buffer album-buffer
        jao-random-album-add-tracks-and-play add-and-play
        jao-random-album-stop stop
        jao-random-album-notify-icon icon))


(provide 'jao-random-album)
;;; jao-random-album.el ends here
