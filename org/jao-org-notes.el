;;; jao-org-notes.el --- A simple system for org note taking  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  jao

;; Author: jao <mail@jao.io>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A note per file

;;; Code:

(require 'org)

(defvar jao-org-notes-dir (expand-file-name "notes" org-directory))

(defun jao-org-notes--insert-title ()
  (let ((title (read-string "Title: ")))
    (when (not (string-empty-p title))
      (let* ((base (replace-regexp-in-string " +" "-" (downcase title)))
             (fname (expand-file-name (concat base ".org") jao-org-notes-dir))
             (exists? (file-exists-p fname)))
        (find-file fname)
        (when (not exists?)
          (insert "#+title: " title "\n")
          t)))))

(defun jao-org-notes--insert-tags ()
  (let ((ts (completing-read-multiple "Tags: "
                                      (org-global-tags-completion-table))))
    (insert "#+filetags:" ":" (mapconcat 'identity ts ":") ":\n")))

(defun jao-org-notes--insert-date ()
  (insert "#+date: ")
  (org-insert-time-stamp (current-time))
  (insert "\n"))

(defun jao-org-notes--template (k)
  `(,k "Note" plain (file jao-org-notes-open) "* %a "))

;;;###autoload
(defun jao-org-notes-open ()
  (interactive)
  (when (jao-org-notes--insert-title)
    (jao-org-notes--insert-date)
    (jao-org-notes--insert-tags)
    (insert "#+link: "))
  (save-buffer)
  (buffer-file-name))

;;;###autoload
(defun jao-org-notes-setup (mnemonic)
  (setq org-capture-templates
        (add-to-list 'org-capture-templates (jao-org-notes--template mnemonic)))
  (add-to-list 'org-agenda-files jao-org-notes-dir)
  (when (fboundp 'org-capture-upgrade-templates)
    (org-capture-upgrade-templates org-capture-templates)))

;;;###autoload
(defun jao-org-notes-backlinks ()
  (interactive)
  (counsel-ag (buffer-name) jao-org-notes-dir))

(provide 'jao-org-notes)
;;; jao-org-notes.el ends here
