(require 'jao-org-utils)
(require 'pdf-info)

(defvar jao-org--sink-dir "./")
(defvar jao-org-open-pdf-fun 'jao-org--pdf-tools-open)

(defun jao-org--pdf-tools-open (path page &optional height)
  (org-open-file path 1)
  (pdf-view-goto-page page)
  (when height
    (image-set-window-vscroll
     (round (/ (* height (cdr (pdf-view-image-size))) (frame-char-height))))))

(defun jao-org--pdf-open (path page &optional height)
  (funcall (or jao-org-open-pdf-fun 'jao-org--pdf-tools-open) path page height))

(defun jao-org--pdf-p (file) (string-match-p ".*\\.pdf$" file))

(defun jao-org-links--open-pdf (link)
  "Open LINK in pdf-view-mode."
  (require 'pdf-tools)
  (cond ((string-match "\\(.*\\)::\\([0-9]*\\)\\+\\+\\([[0-9]\\.*[0-9]*\\)"  link)
         (let* ((path (match-string 1 link))
                (page (string-to-number (match-string 2 link)))
                (height (string-to-number (match-string 3 link))))
           (jao-org--pdf-open path page height)))
        ((string-match "\\(.*\\)::\\([0-9]+\\)$"  link)
         (let* ((path (match-string 1 link))
                (page (string-to-number (match-string 2 link))))
           (jao-org--pdf-open path page)))
        (t (org-open-file link 1))))

(defun jao-org-links--follow-doc (link)
  (let* ((full-link (concat org-directory "/doc/" link))
         (dest-path (car (split-string full-link "::"))))
    (when (not (file-exists-p dest-path))
      (let* ((sink-file (expand-file-name link jao-org--sink-dir))
             (real-file (if (file-exists-p sink-file) sink-file
                          (read-file-name "Import file: "
                                          jao-org--sink-dir link link))))
        (shell-command (format "mv %s %s" real-file dest-path))))
    (if (jao-org--pdf-p dest-path)
        (jao-org-links--open-pdf full-link)
        (browse-url (format "file://%s" (expand-file-name  dest-path))))))

(defun jao-org-links--complete-doc (&optional arg)
  (let ((default-directory jao-org--sink-dir))
    (let ((f (replace-regexp-in-string "^file:" "doc:"
                                       (org-file-complete-link arg))))
      (if (jao-org--pdf-p f)
          (concat f "::" (read-from-minibuffer "Page: " "1"))
        f))))

(defsubst jao-org--title->file (title)
  (concat (mapconcat 'downcase (split-string title nil t) "-") ".pdf"))

(defun jao-org--pdf-title (&optional fname)
  (let ((base (file-name-base (or fname (pdf-view-buffer-file-name)))))
    (capitalize (replace-regexp-in-string "-" " " base))))

(defvar-local jao--pdf-outline nil)

(defun jao-org--pdf-section-title (&optional page)
  (when (not jao--pdf-outline)
    (setq-local jao--pdf-outline (pdf-info-outline)))
  (let ((page (or page (pdf-view-current-page)))
        (outline jao--pdf-outline)
        (cur-page 0)
        (cur-title (jao-org--pdf-title)))
    (while (and (car outline) (< cur-page page))
      (setq cur-page (cdr (assoc 'page (car outline))))
      (when (<= cur-page page)
        (setq cur-title (cdr (assoc 'title (car outline)))))
      (setq outline (cdr outline)))
    (replace-regexp-in-string "[[:blank:]]+" " " cur-title)))

;;;###autoload
(defvar jao-org-links-pdf-store-fun nil)

(defun jao-org-links--store-pdf-link ()
  (or (when (fboundp jao-org-links-pdf-store-fun)
        (funcall jao-org-links-pdf-store-fun))
      (when (derived-mode-p 'pdf-view-mode)
        (jao-org-links-store-pdf-link buffer-file-name
                                      (pdf-view-current-page)
                                      (jao-org--pdf-section-title)))))

;;;###autoload
(defun jao-org-links-store-pdf-link (path page title)
  (org-store-link-props
   :type "doc"
   :link (format "doc:%s::%d" (file-name-nondirectory path) page)
   :description (format "%s (p. %d)" title page)))

;;;###autoload
(defun jao-org-insert-doc (title)
  (interactive "sDocument title: ")
  (insert (format "[[doc:%s][%s]]" (jao-org--title->file title) title)))

;;;###autoload
(defun jao-org-links-setup (sink-dir)
  (interactive)
  (org-link-set-parameters "doc"
                           :follow #'jao-org-links--follow-doc
                           :complete #'jao-org-links--complete-doc
                           :store #'jao-org-links--store-pdf-link)
  (setq jao-org--sink-dir (file-name-as-directory sink-dir)))

(defun jao-org--open-other (otp)
  (let ((rxs ))))

;;;###autoload
(defun jao-org-org-to-pdf-file ()
  (replace-regexp-in-string "/notes/\\(.+\\)\\.org$"
                            "/doc/\\1.org"
                            buffer-file-name))

;;;###autoload
(defun jao-org-pdf-to-org-file (&optional file-name)
  (replace-regexp-in-string "/doc/\\(.+\\)\\.pdf$"
                            "/notes/\\1.org"
                            (or file-name buffer-file-name)))

;;;###autoload
(defun jao-org-insert-doc-skeleton (&optional title)
  (insert "#+title: " (or title (jao-org--pdf-title (buffer-file-name)))
          "\n#+author:\n#+startup: latexpreview\n\n"))

;;;###autoload
(defun jao-org-pdf-goto-org (arg)
  (interactive "P")
  (when (jao-org--pdf-p buffer-file-name)
    (let* ((file (jao-org-pdf-to-org-file))
           (new (not (file-exists-p file)))
           (title (jao-org--pdf-title)))
      (when (or arg new) (org-store-link nil t))
      (find-file-other-window file)
      (when new
        (jao-org-insert-doc-skeleton title)
        (org-insert-link)))))

;;;###autoload
(defun jao-org-pdf-goto-org* ()
  (interactive)
  (jao-org-pdf-goto-org t))

(provide 'jao-org-links)
