;;; jao-ednc.el --- Minibuffer notifications using EDNC  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  jao

;; Author: jao <mail@jao.io>
;; Keywords: tools, abbrev

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;  Use the ednc package to provide a notification daemon that uses
;;  the minibuffer to display them.

;;; Code:

(require 'ednc)
(require 'jao-minibuffer)

(declare-function tracking-add-buffer "tracking")
(declare-function tracking-remove-buffer "tracking")

(defvar jao-ednc--count-format " {%d} ")
(defvar jao-ednc--notifications ())
(defvar jao-ednc--handlers ())

(defvar jao-ednc-use-minibuffer-notifications nil)
(defvar jao-ednc-use-tracking t)

(defface jao-ednc-tracking '((t :inherit warning))
  "Tracking notifications face"
  :group 'jao-ednc)

(defun jao-ednc--last-notification () (car jao-ednc--notifications))

(defun jao-ednc--format-last ()
  (when (jao-ednc--last-notification)
    (let ((s (ednc-format-notification (jao-ednc--last-notification) t)))
      (replace-regexp-in-string "\n" " " (substring-no-properties s)))))

(defun jao-ednc--count ()
  (let ((no (length jao-ednc--notifications)))
    (if (> no 0)
        (propertize (format jao-ednc--count-format no) 'face 'warning)
      "")))

(defun jao-ednc-add-handler (app handler)
  (add-to-list 'jao-ednc--handlers (cons app handler)))

(defun jao-ednc-ignore-app (app)
  (jao-ednc-add-handler app (lambda (not _) (ednc-dismiss-notification not))))

(defun jao-ednc--pop-minibuffer ()
  (if jao-ednc-use-minibuffer-notifications
      (jao-minibuffer-pop-notification)
    (jao-minibuffer-refresh)))

(defun jao-ednc--clean (&optional notification)
  (tracking-remove-buffer (get-buffer ednc-log-name))
  (if notification
      (remove notification jao-ednc--notifications)
    (pop jao-ednc--notifications))
  (jao-ednc--pop-minibuffer))

(defun jao-ednc--show-last ()
  (if jao-ednc-use-minibuffer-notifications
      (jao-minibuffer-push-notification '(:eval (jao-ednc--format-last)))
    (message "%s" (jao-ednc--format-last))))

(defun jao-ednc--default-handler (notification newp)
  (if (not newp)
      (jao-ednc--clean notification)
    (tracking-add-buffer (get-buffer ednc-log-name) '(jao-ednc-tracking))
    (push notification jao-ednc--notifications)
    (jao-ednc--show-last)))

(defun jao-ednc--handler (notification)
  (alist-get (ednc-notification-app-name notification)
             jao-ednc--handlers
             #'jao-ednc--default-handler
             nil
             'string=))

(defun jao-ednc--on-notify (old new)
  (when old (funcall (jao-ednc--handler old) old nil))
  (when new (funcall (jao-ednc--handler new) new t)))

;;;###autoload
(defun jao-ednc-setup (minibuffer-order)
  (setq jao-notify-use-messages-p t)
  (with-eval-after-load "tracking"
    (when jao-ednc-use-tracking
      (add-to-list 'tracking-faces-priorities 'jao-ednc-tracking)
      (when (listp tracking-shorten-modes)
        (add-to-list 'tracking-shorten-modes 'ednc-view-mode))))
  (jao-minibuffer-add-variable '(jao-ednc--count) minibuffer-order)
  (add-hook 'ednc-notification-presentation-functions #'jao-ednc--on-notify)
  (ednc-mode))

;;;###autoload
(defun jao-ednc-pop ()
  (interactive)
  (pop-to-buffer-same-window ednc-log-name))

;;;###autoload
(defun jao-ednc-show ()
  (interactive)
  (if (not (jao-ednc--last-notification))
      (jao-ednc-pop)
    (jao-ednc--show-last)))

;;;###autoload
(defun jao-ednc-invoke-last-action ()
  (interactive)
  (if (jao-ednc--last-notification)
      (ednc-invoke-action (jao-ednc--last-notification))
    (message "No active notifications"))
  (jao-ednc--clean))

;;;###autoload
(defun jao-ednc-dismiss ()
  (interactive)
  (when (jao-ednc--last-notification)
    (ignore-errors
      (with-current-buffer ednc-log-name
        (ednc-dismiss-notification (jao-ednc--last-notification)))))
  (jao-ednc--clean))

(provide 'jao-ednc)
;;; jao-ednc.el ends here
