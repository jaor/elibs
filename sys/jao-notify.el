;; jao-notify.el -- Interacting with notification daemon

;; Copyright (c) 2017, 2019, 2020 Jose Antonio Ortega Ruiz

;; Author: Jose Antonio Ortega Ruiz <jao@gnu.org>
;; Start date: Sun Jan 08, 2017 20:24


;;; Comentary:

;; Simple notifications using echo or dbus notifications

;;; Code:

(defvar jao-notify-use-messages-p nil)
(defvar jao-notify-timeout 5000)

(declare-function notifications-notify "notifications")

(defun jao-notify (msg &optional title icon)
  (if jao-notify-use-messages-p
      (message "%s%s%s" (or title "") (if title ": " "") (or msg ""))
      (let* ((args `(:timeout ,jao-notify-timeout))
             (args (append args
                           (if title `(:title ,title :body ,msg) `(:title ,msg))))
             (args (if (and (stringp icon) (file-exists-p icon))
                       (append args `(:app-icon ,(format "%s" icon)))
                     args)))
        (apply 'notifications-notify args))))


(provide 'jao-notify)
;;; jao-notify.el ends here
